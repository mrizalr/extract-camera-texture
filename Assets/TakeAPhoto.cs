﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TakeAPhoto : MonoBehaviour
{

    static WebCamTexture webCamTexture;
    public Texture2D _frame;

    // Start is called before the first frame update
    void Start()
    {
        applyCamera();
    }

    private void applyCamera()
    {
        if (webCamTexture == null)
        {
            webCamTexture = new WebCamTexture(WebCamTexture.devices[1].name, 4128, 3096, 60);
        }
        
        GetComponent<Image>().material.mainTexture = webCamTexture;
        
        if (!webCamTexture.isPlaying)
            webCamTexture.Play();
    }

    public void onTakeScreenshot()
    {
        TakeScreenshot();
    }

    public void onTakePhoto()
    {
        StartCoroutine(TakePhoto());
    }

    public void TakeScreenshot()
    {
        string imageName = "screenshot.png";

        // Take the screenshot
        ScreenCapture.CaptureScreenshot(imageName);
        Debug.Log("Screenshot Captured");
    }

    public IEnumerator TakePhoto()
    {
        yield return new WaitForEndOfFrame();
        Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
        photo.SetPixels(webCamTexture.GetPixels());
        photo.Apply();

        OverlayImages(photo, _frame);
    }

    public void OverlayImages(Texture2D _photo, Texture2D frame)
    {
        int offset = frame.width - _photo.width;
        Texture2D _newFrame = new Texture2D(frame.width, frame.height);

        for (int i = 0; i < _newFrame.width; i++)
        {
            for (int j = 0; j < _newFrame.height; j++)
            {
                if ((i > offset / 2 && i < _newFrame.width - offset / 2) &&
                    (j > offset / 2 && j < _newFrame.height - offset / 2))
                {
                    _newFrame.SetPixel(i, j, _photo.GetPixel(i-offset/2, j-offset/2));
                }
                else
                {
                    _newFrame.SetPixel(i, j, frame.GetPixel(i,j));
                }

            }
        }
        _newFrame.Apply();
        ExtractPhoto(_newFrame);
    }

    public void ExtractPhoto(Texture2D photo)
    {
        byte[] bytes = photo.EncodeToPNG();
        File.WriteAllBytes("E:/unity project/Testing photo/hasilfoto.PNG", bytes);
        Debug.Log("Photo Captured");
    }
}
